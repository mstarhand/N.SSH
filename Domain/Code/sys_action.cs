﻿﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

namespace Domain
{
    /// <summary>sys_action</summary>
    /// <remarks></remarks>
    public partial class sys_action 
    {
        #region 对象操作﻿

        #endregion

        #region 扩展属性﻿
        public virtual IList<sys_role> Roles { get; set; }
        #endregion

        #region 扩展查询﻿

        #endregion

        #region 高级查询
  
        #endregion

        #region 扩展操作
        #endregion

        #region 业务
        #endregion
    }
}
