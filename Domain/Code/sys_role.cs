﻿﻿
using Domain.IDomain.IDomian;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

namespace Domain
{
    /// <summary>sys_role</summary>
    /// <remarks></remarks>
    public partial class sys_role 
    {
  
        #region 对象操作﻿

        #endregion

        #region 扩展属性﻿
        private IList<sys_user> m_users;

        public virtual IList<sys_user> Users 
        {
            get 
            {
                if (this.m_users == null)
                {
                    this.m_users = new List<sys_user>();
                }
                return this.m_users;
            }
            set 
            {
                this.m_users = value;
            }
        }
        private IList<sys_action> m_actions;
        public virtual IList<sys_action> Actions 
        {
              get{
                if (this.m_actions == null)
                {
                    this.m_actions = new List<sys_action>();
                }
                return this.m_actions;
            }
            set 
            {
                this.m_actions = value;
            }
        }

        #endregion

        #region 扩展查询﻿

        #endregion

        #region 高级查询
  
        #endregion

        #region 扩展操作
        #endregion

        #region 业务
        #endregion

       
    }
}
