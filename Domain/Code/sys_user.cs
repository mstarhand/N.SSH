﻿﻿
using Domain.IDomain.IDomian;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

namespace Domain
{
    /// <summary>sys_user</summary>
    /// <remarks></remarks>
    public partial class sys_user 
    {
        #region 对象操作﻿

        #endregion

        #region 扩展属
        private IList<sys_role> m_roles;

        public virtual IList<sys_role> Roles
        {
            get
            {
                if (this.m_roles == null)
                {
                    this.m_roles = new List<sys_role>();
                }
                return this.m_roles;
            }
            set
            {
                this.m_roles = value;
            }
        }
        
        #endregion

        #region 扩展查询﻿

        #endregion

        #region 高级查询
  
        #endregion

        #region 扩展操作
        #endregion

        #region 业务
        #endregion



    }
}
