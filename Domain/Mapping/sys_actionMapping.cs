﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    public class sys_actionMapping : ClassMap<sys_action>
    {
        public sys_actionMapping()
        {
            //指定持久化类对应的数据表
            //Table("sys_user");
            //自动增长的id
            Id(i => i.id).GeneratedBy.Native();
            //映射关系
            //Id<Guid>("CustomerID").GeneratedBy.Guid();
            Map(m => m.name);
            Map(m => m.action);
            Map(m => m.mark_id);

            Map(m => m.create_time);
            Map(m => m.update_time);
        }
    }
}
