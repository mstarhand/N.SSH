﻿using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.IDomain.IDomian
{
    /// <summary>sys_user接口</summary>
    /// <remarks></remarks>
    public partial interface Isys_user:IEntity
    {
        #region 属性
         /// <summary></summary>
         Int32 id { get; set; }
         /// <summary></summary>
         String name { get; set; }
         /// <summary></summary>
         String login_name { get; set; }
         /// <summary></summary>
         String pwd { get; set; }
         /// <summary></summary>
         String email { get; set; }
         /// <summary></summary>
         DateTime create_time { get; set; }
         /// <summary></summary>
         DateTime update_time { get; set; }
        #endregion
        Isys_user_ _X { get; }
     }
      public interface Isys_user_
        {
            /// <summary></summary>
            Field id { get;}
            /// <summary></summary>
            Field name { get;}
            /// <summary></summary>
            Field login_name { get;}
            /// <summary></summary>
            Field pwd { get;}
            /// <summary></summary>
            Field email { get;}
            /// <summary></summary>
            Field create_time { get;}
            /// <summary></summary>
            Field update_time { get;}
        }
}