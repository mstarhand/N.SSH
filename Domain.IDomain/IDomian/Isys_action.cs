﻿using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.IDomain.IDomian
{
    /// <summary>sys_action接口</summary>
    /// <remarks></remarks>
    public partial interface Isys_action:IEntity
    {
        #region 属性
         /// <summary></summary>
         Int32 id { get; set; }
         /// <summary></summary>
         String name { get; set; }
         /// <summary></summary>
         String action { get; set; }
         /// <summary></summary>
         Int32 mark_id { get; set; }
         /// <summary></summary>
         DateTime create_time { get; set; }
         /// <summary></summary>
         DateTime update_time { get; set; }
        #endregion
        Isys_action_ _X { get; }
     }
      public interface Isys_action_
        {
            /// <summary></summary>
            Field id { get;}
            /// <summary></summary>
            Field name { get;}
            /// <summary></summary>
            Field action { get;}
            /// <summary></summary>
            Field mark_id { get;}
            /// <summary></summary>
            Field create_time { get;}
            /// <summary></summary>
            Field update_time { get;}
        }
}