﻿using NHibernate;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Code.Hibernate
{
    public interface IEntitySessionNhibernate<TEntity> : IEntitySession<TEntity> where TEntity : IEntity
    {
        ISession getISession{get;}
        ICriteria CreateCriteria();
    }
}
