﻿
using NHibernate.Criterion;
using PX.Code.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Code.Hibernate
{
    public static  class ProjectionsExtend
    {
        public static PropertyProjection ToProperty(this Field f)
        {
            return Projections.Property(f.Name);
        }
        public static AggregateProjection ToAvg(this Field f)
        {
            return Projections.Avg(f.Name);
        }
        public static CountProjection ToCount(this Field f)
        {
            return Projections.Count(f.Name);
        }
        public static AggregateProjection ToMax(this Field f)
        {
            return Projections.Max(f.Name);
        }
        public static AggregateProjection ToMin(this Field f)
        {
            return Projections.Min(f.Name);
        }
        public static AggregateProjection ToSum(this Field f)
        {
            return Projections.Sum(f.Name);
        }
        public static Order ToDesc(this Field f)
        {
            return Order.Desc(f.Name);
        }
        public static Order ToAsc(this Field f)
        {
            return Order.Asc(f.Name);
        }
    }
}
