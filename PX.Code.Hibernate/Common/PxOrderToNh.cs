﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using PX.Core;

namespace PX.Code.Hibernate.Common
{
    public static class PxOrderToNh
    {
        public static Order ToOrderNh(this PxOrder cri)
        {
            if (cri == null) { return null; }
            Order order = null;
            order = new Order(cri.propertyName, cri.ascending);
            return order;
        }
    }
}
