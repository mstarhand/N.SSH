﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Code.Base
{
    /// <summary>指定实体类所绑定的数据表信息。</summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityAttribute:Attribute
    {
        private String _Name;
        /// <summary>
        /// 表名。
        /// </summary>
        public String Name { get { return _Name; } set { _Name = value; } }

        private String _Description;
        /// <summary>描述</summary>
        public String Description { get { return _Description; } set { _Description = value; } }

        private String _ConnName;
        /// <summary>
        /// 连接名。
        /// 实体类的所有数据库操作，将发生在该连接名指定的数据库连接上。
        /// 此外，可动态修改实体类在当前线程上的连接名（改Meta.ConnName）；
        /// 也可以在配置文件中通过XCode.ConnMaps把连接名映射到别的连接上。
        /// </summary>
        public String ConnName { get { return _ConnName; } set { _ConnName = value; } }
    }
}
