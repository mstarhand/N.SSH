﻿using System;
using PX.Code.Base.Entity;
using PX.Core;
namespace PX.Code.Base
{
    public interface IEntityDAO<TEntity>
     where TEntity :IEntity
    {
        void BeginTrans();
        void Commit();
        bool Delete(TEntity t);
        int DeleteAll(AbstractExpressions icriterion);
        TEntity Find(Field field, object obj);
        TEntity Find(PX.Code.Base.Field[] fields, object[] obj);
        TEntity Find(PX.Core.AbstractExpressions icriterion);
        TEntity Find(System.Collections.Generic.IList<PX.Code.Base.Field> fields, object[] obj);
        TEntity Find(string field, object obj);
        TEntity Find(string[] fields, object[] obj);
        IEntityList<TEntity> FindAll();
        IEntityList<TEntity> FindAll(PX.Code.Base.Field field, object obj);
        IEntityList<TEntity> FindAll(PX.Code.Base.Field[] fields, object[] obj);
        IEntityList<TEntity> FindAll(PX.Core.AbstractExpressions icriterion);
        IEntityList<TEntity> FindAll(PX.Core.AbstractExpressions icriterion, PX.Core.PxOrder order);
        IEntityList<TEntity> FindAll(PX.Core.AbstractExpressions icriterion, PX.Core.PxOrder order, int startRowIndex, int maximumRows, out int rowCount);
        IEntityList<TEntity> FindAll(System.Collections.Generic.IList<PX.Code.Base.Field> fields, object[] obj);
        IEntityList<TEntity> FindAll(string field, object obj);
        IEntityList<TEntity> FindAll(string icriterion, PX.Core.PxOrder order, int startRowIndex, int maximumRows, out int rowCount);
        IEntityList<TEntity> FindAll(string strWhere);
        IEntityList<TEntity> FindAll(string[] fields, object[] obj);
        TEntity FindByID(object id);
        int Insert(TEntity t);
        TEntity LoadData(System.Data.DataRow dr);
        void Rollback();
        bool Save(TEntity t);
        bool Update(TEntity t);
    }
}
