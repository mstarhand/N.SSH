﻿using System;
namespace PX.Code.Base.Entity
{
    public interface ITableItem
    {
        Type EntityType { get; set; }
        System.Collections.Generic.List<string> FieldNames { get; set; }
        System.Collections.Generic.List<Field> Fields { get; set; }
        Field FindByName(string name);
        string Name { get; set; }
        Field Unique { get; }
    }
}
