﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Code.Base.Entity
{
    /// <summary>
    /// 数据实体接口
    /// </summary>
    public interface  IEntity
    {
        #region 操作
        /// <summary>把该对象持久化到数据库</summary>
        /// <returns></returns>
        Int32 Insert();

        /// <summary>更新数据库</summary>
        /// <returns></returns>
        Boolean Update();

        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        Boolean Delete();

        /// <summary>保存。根据主键检查数据库中是否已存在该对象，再决定调用Insert或Update</summary>
        /// <returns></returns>
        Boolean Save();

        ///// <summary>克隆实体。创建当前对象的克隆对象，仅拷贝基本字段</summary>
        ///// <param name="setDirty">是否设置脏数据</param>
        ///// <returns></returns>
        //IEntity CloneEntity(Boolean setDirty = true);
        #endregion

        #region 获取/设置 字段值
        /// <summary>获取/设置 字段值。</summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        Object this[String name] { get; set; }

        #endregion
    }
}
