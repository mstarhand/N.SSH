﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Context;
using Spring.Context.Support;

namespace Service.Common
{
    public class ServiceCommon
    {

        public static IApplicationContext ctx = ContextRegistry.GetContext();
        public static Object GetObject(string name)
        {
            return ctx.GetObject(name);
        }
        public static T GetObject<T>(string name)
        {
            return (T)ctx.GetObject(name);
        }
    }
}
