﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Code.Core.EntityDAO
{
    /// <summary>数据实体基类的基类</summary>
    [Serializable]
    public abstract class EntityDAOBase<TEntity>
    {
       
        #region 操作
        /// <summary>把该对象持久化到数据库</summary>
        /// <returns></returns>
        public abstract Int32 Insert(TEntity t);

        /// <summary>更新数据库</summary>
        /// <returns></returns>
        public abstract Boolean Update(TEntity t);

        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        public abstract Boolean Delete(TEntity t);

        /// <summary>保存。根据主键检查数据库中是否已存在该对象，再决定调用Insert或Update</summary>
        /// <returns></returns>
        public abstract Boolean Save(TEntity t);

  
        #endregion

    }
}
