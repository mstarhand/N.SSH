﻿using NewLife.Configuration;
using PX.Code.Base;
using PX.Code.Base.Common;
using PX.Code.Base.Entity;
using PX.Code.Hibernate;
using PX.Code.Hibernate.Common;
using PX.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PX.Code.Core.EntityDAO
{
    /// <summary>数据实体类基类。所有数据实体类都必须继承该类。</summary>
    [Serializable]
    public partial class EntityDAO<TEntity> : EntityDAOBase<TEntity>,IEntityDAO<TEntity> where TEntity :class, IEntity, new()
    {

        private static String _ConnName;
        /// <summary>链接名。线程内允许修改，修改者负责还原。若要还原默认值，设为null即可</summary>
        public static String ConnName
        {

            get { return _ConnName ?? (_ConnName = typeof(TEntity).GetCustomAttribute<EntityAttribute>(true).ConnName); }
            set
            {
                _ConnName = value;
            }
        }
        private static IEntitySessionNhibernate<TEntity> _entitySession = new EntitySessionNhibernate<TEntity>(ConnName);
        public static IEntitySessionNhibernate<TEntity> entitySession 
        {
            get 
            {
                return _entitySession; 
            }
        }
        public IEntitySessionNhibernate<TEntity> Session
        {
            get {return entitySession;}
        }
        public static Field FindFieldByName(string name)
        {
            return entitySession.TableItem.FindByName(name);
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        /// <returns></returns>
        public override Int32 Insert(TEntity t)
        {
            return (Int32)entitySession.Insert(t);
        }

        /// <summary>更新数据库</summary>
        /// <returns></returns>
        public override Boolean Update(TEntity t)
        {
           return entitySession.Update(t);
        }

        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        public override Boolean Delete(TEntity t)
        {
            return entitySession.Delete(t);
        }

        /// <summary>检查这个对象是否已经存在Session中。如果对象不在，调用Save(object)来保存。如果对象存在，检查这个对象是否改变了。如果对象改变，调用Update(object)来更新。</summary>
        /// <returns></returns>
        public override Boolean Save(TEntity t)
        {
            return entitySession.Save(t);
        }
        #region 事务
        public void BeginTrans()
        {
            entitySession.BeginTrans();
        }
        public void Commit()
        {
            entitySession.Commit();
        }
        public void Rollback()
        {
            entitySession.Rollback();
        }
        #endregion
        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        public  Int32 DeleteAll(AbstractExpressions icriterion)
        {
            return entitySession.DeleteAll(icriterion);
        }
        public  IEntityList<TEntity> FindAll()
        {
            return entitySession.FindAll();
        }
        public  IEntityList<TEntity> FindAll(String strWhere)
        {
            return entitySession.FindAll(strWhere, string.Empty);
        }
        public  IEntityList<TEntity> FindAll(Field field, object obj)
        {
            return entitySession.FindAll(field, obj);
        }
        public  IEntityList<TEntity> FindAll(Field[] fields, object[] obj)
        {
            return entitySession.FindAll(fields, obj);
        }
        public  IEntityList<TEntity> FindAll(IList<Field> fields, object[] obj)
        {
            return entitySession.FindAll(fields, obj);
        }
        public  IEntityList<TEntity> FindAll(String field, object obj)
        {
            Field fieldtemp = entitySession.TableItem.FindByName(field);
            return entitySession.FindAll(fieldtemp, obj);
        }
        public  IEntityList<TEntity> FindAll(String[] fields, object[] obj)
        {
            List<Field> list = new List<Field>();
            for (int i = 0; i < fields.Length; i++)
            {
                list.Add(entitySession.TableItem.FindByName(fields[i]));
            }
            return entitySession.FindAll(list, obj);
        }
        public  IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order)
        {
            return entitySession.FindAll(icriterion, order, 0, int.MaxValue);
        }
        public  IEntityList<TEntity> FindAll(AbstractExpressions icriterion)
        {
            return entitySession.FindAll(icriterion, null, 0, int.MaxValue);
        }
        public  IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
        {
            rowCount = entitySession.FindCount(icriterion);
            return entitySession.FindAll(icriterion, order, startRowIndex, maximumRows);
        }
        public  IEntityList<TEntity> FindAll(String icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
        {
            rowCount = entitySession.FindCount(icriterion);
            return entitySession.FindAll(icriterion, order.ToString(), startRowIndex, maximumRows);
        }
        public  TEntity Find(Field field, object obj)
        {
            return entitySession.Find(field, obj);
        }
        public  TEntity Find(Field[] fields, object[] obj)
        {
            return entitySession.Find(fields, obj);
        }
        public  TEntity Find(IList<Field> fields, object[] obj)
        {
            return entitySession.Find(fields, obj);
        }
        public  TEntity Find(String field, object obj)
        {
            Field fieldtemp = entitySession.TableItem.FindByName(field);
            return entitySession.Find(fieldtemp, obj);
        }
        public  TEntity Find(String[] fields, object[] obj)
        {
            List<Field> list = new List<Field>();
            for (int i = 0; i < fields.Length; i++)
            {
                list.Add(entitySession.TableItem.FindByName(fields[i]));
            }
            return entitySession.Find(list, obj);
        }
        public  TEntity Find(AbstractExpressions icriterion)
        {
            return entitySession.Find(icriterion);
        }
        public  TEntity FindByID(object id)
        {
            return entitySession.FindByID(id);
        }
        public  TEntity LoadData(DataRow dr)
        {
            return dr.PxToEntity<TEntity>();
        }
        

    }
}
