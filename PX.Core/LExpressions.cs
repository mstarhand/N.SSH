﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public  class LExpressions:AbstractExpressions
    {
        public override List<IExpression> ExpressionList{  get;set; }
        public override List<IOperatorExpression> OperatorExpressionList { get; set; }
        public LExpressions()
        {
            ExpressionList = new List<IExpression>();
            OperatorExpressionList = new List<IOperatorExpression>();
        }
    }
}
