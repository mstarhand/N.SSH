using System;
using System.Collections.Generic;
using System.Text;


namespace PX.Core
{
	/// <summary>
	/// Represents an order imposed upon a <see cref="ICriteria"/>
	/// result set.
	/// </summary>
	/// <remarks>
	/// Should Order implement ICriteriaQuery?
	/// </remarks>
	[Serializable]
	public class PxOrder
	{
		public bool ascending;
		public string propertyName;

		public PxOrder(string propertyName, bool ascending)
		{
			this.propertyName = propertyName;
			this.ascending = ascending;
		}

		public override string ToString()
		{
			return  propertyName + (ascending ? " asc" : " desc");
		}

		/// <summary>
		/// Ascending order
		/// </summary>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static PxOrder Asc(string propertyName)
		{
			return new PxOrder(propertyName, true);
		}



		/// <summary>
		/// Descending order
		/// </summary>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static PxOrder Desc(string propertyName)
		{
			return new PxOrder(propertyName, false);
		}

	}
}
