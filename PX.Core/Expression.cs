﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public class Expression:IExpression
    {
        /// <summary>
        /// 条件中对应的列名
        /// </summary>
        public string ColumnName { get; set; }

        public string ColumnName2 { get; set; }
        /// <summary>
        /// 列所对应的值
        /// </summary>
        public object ParamStartValue { get; set; }
    
        /// <summary>
        /// 比较运算符：等于 大于 小于等
        /// </summary>
        public ComparisonWhere ComparisonWhere { get; set; }

        /// <summary>
        /// 用与Between ParamStartValue and ParamEndValue，
        /// 如果没有使用Between 该参数可以不赋值
        /// </summary>
        public object ParamEndValue { get; set; }

        public object[] ParamValues { get; set; }
    }

}
