﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{

    /// <summary>
    /// 运算符
    /// </summary>
    public enum OperatorWhere
    {
        And,
        Or
    }

    /// <summary>
    /// 比较运算符
    /// </summary>
    public enum ComparisonWhere
    {
        BetweenAnd,//在两者之间
        Equals,//等于
        GreaterOrEquals,//大于等于
        GreaterThan,//大于
        IsNull,//空
        IsNotNull,//非空
        LessOrEquals,//小于等于
        LessThan,//小于
        LikeAnyWhere,//模糊查询
        LikeStart,
        LikeEnd,
        NotEquals,//不等于
        In,
        NotIn,
        EqualProperty,
        NotEqProperty,
        GtProperty,
        GeProperty,
        LtProperty,
        LeProperty,
        IsNotEmpty,
        IsEmpty

    }
    public class EnumCommon
    {
    }
}
