using System.Collections.Generic;



namespace PX.Criterion
{
	/// <summary>
	/// An object-oriented representation of a query criterion that may be used as a constraint
	/// in a <see cref="ICriteria"/> query.
	/// </summary>
	/// <remarks>
	/// Built-in criterion types are provided by the <c>Expression</c> factory class.
	/// This interface might be implemented by application classes but, more commonly, application 
	/// criterion types would extend <c>AbstractCriterion</c>.
	/// </remarks>
	public interface ICriterion
	{
	
	}
}
