using System;
using System.Collections.Generic;



namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that represents "null" constraint.
	/// </summary>
	[Serializable]
	public class NullExpression : AbstractCriterion
	{
		private readonly string _propertyName;
		private readonly IProjection _projection;
	

		/// <summary>
		/// Initializes a new instance of the <see cref="NullExpression"/> class.
		/// </summary>
		/// <param name="projection">The projection.</param>
		public NullExpression(IProjection projection)
		{
			_projection = projection;
		}

		/// <summary>
		/// Initialize a new instance of the <see cref="NotNullExpression" /> class for a named
		/// Property that should be null.
		/// </summary>
		/// <param name="propertyName">The name of the Property in the class.</param>
		public NullExpression(string propertyName)
		{
			_propertyName = propertyName;
		}


	

		/// <summary></summary>
		public override string ToString()
		{
			return (_projection ?? (object)_propertyName) + " is null";
		}
	}
}
