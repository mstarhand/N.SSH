using System;
using System.Collections.Generic;


namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that represents "not null" constraint.
	/// </summary>
	[Serializable]
	public class NotNullExpression : AbstractCriterion
	{
		private readonly string _propertyName;
		private IProjection _projection;

		/// <summary>
		/// Initializes a new instance of the <see cref="NotNullExpression"/> class.
		/// </summary>
		/// <param name="projection">The projection.</param>
		public NotNullExpression(IProjection projection)
		{
			_projection = projection;
		}



		/// <summary>
		/// Initialize a new instance of the <see cref="NotNullExpression" /> class for a named
		/// Property that should not be null.
		/// </summary>
		/// <param name="propertyName">The name of the Property in the class.</param>
		public NotNullExpression(string propertyName)
		{
			_propertyName = propertyName;
		}
		public override string ToString()
		{
			return (_projection ?? (object)_propertyName) + " is not null";
		}
	}
}
