﻿using System;
namespace PX.Core
{
    public interface IOperatorExpression
    {
        OperatorWhere OperatorWhere { get; set; }
    }
}
