﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public  class RestrictionsFactory
    {
        /// <summary>
        /// Apply an "equal" constraint to the identifier property
        /// </summary>
        /// <param name="value"></param>
        /// <returns>ICriterion</returns>
        public static SimpleExpressions IdEq(object value)
        {
            return new SimpleExpressions("ID", value,ComparisonWhere.Equals);
        }
        /// <summary>
        /// Apply an "equal" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions Eq(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value,ComparisonWhere.Equals);
        }
        /// <summary>
        /// Apply an "equal" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions NotEq(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value, ComparisonWhere.NotEquals);
        }
        /// <summary>
        /// Apply a "like" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        /// <returns>A <see cref="LikeExpression" />.</returns>
        public static SimpleExpressions LikeAnyWhere(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value, ComparisonWhere.LikeAnyWhere);
        }

        public static SimpleExpressions LikeStart(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value, ComparisonWhere.LikeStart);
        }
        public static SimpleExpressions LikeEnd(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value, ComparisonWhere.LikeEnd);
        }
     

        /// <summary>
        /// Apply a "greater than" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions Gt(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value,ComparisonWhere.GreaterThan);
        }

        /// <summary>
        /// Apply a "less than" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions Lt(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value,ComparisonWhere.LessThan);
        }

        /// <summary>
        /// Apply a "less than or equal" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions Le(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value,ComparisonWhere.LessOrEquals);
        }

        /// <summary>
        /// Apply a "greater than or equal" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="value">The value for the Property.</param>
        public static SimpleExpressions Ge(string propertyName, object value)
        {
            return new SimpleExpressions(propertyName, value, ComparisonWhere.GreaterOrEquals);
        }

        /// <summary>
        /// Apply a "between" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="lo">The low value for the Property.</param>
        /// <param name="hi">The high value for the Property.</param>
        /// <returns>A <see cref="BetweenExpression" />.</returns>
        public static SimpleExpressions Between(string propertyName, object lo1,object lo2)
        {
            return new SimpleExpressions(propertyName, lo1,lo2, ComparisonWhere.BetweenAnd);
        }

        /// <summary>
        /// Apply an "in" constraint to the named property 
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="values">An array of values.</param>
        /// <returns>An <see cref="InExpression" />.</returns>
        public static SimpleExpressions In(string propertyName, object[] values)
        {
            return new SimpleExpressions(propertyName, values,ComparisonWhere.In);
        }

        /// <summary>
        /// Apply an "in" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="values">An ICollection of values.</param>
        /// <returns>An <see cref="InExpression" />.</returns>
        public static SimpleExpressions In(string propertyName, ICollection values)
        {
            object[] ary = new object[values.Count];
            values.CopyTo(ary, 0);
            return new SimpleExpressions(propertyName, values, ComparisonWhere.In);
        }

        /// <summary>
        /// Apply an "in" constraint to the named property. This is the generic equivalent
        /// of <see cref="In(string, ICollection)" />, renamed to avoid ambiguity.
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <param name="values">An <see cref="System.Collections.Generic.IEnumerable{T}" />
        /// of values.</param>
        /// <returns>An <see cref="InExpression" />.</returns>
        public static SimpleExpressions InG<T>(string propertyName, IEnumerable<T> values)
        {
            var array = new object[values.Count()];
            var i = 0;
            foreach (var item in values)
            {
                array[i] = item;
                i++;
            }
            return new SimpleExpressions(propertyName, values, ComparisonWhere.In);
        }
        /// <summary>
        /// Apply an "is null" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <returns>A <see cref="NullExpression" />.</returns>
        public static SimpleExpressions IsNull(string propertyName)
        {
            return new SimpleExpressions(propertyName, ComparisonWhere.IsNull);
        }

        /// <summary>
        /// Apply an "equal" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="EqPropertyExpression"/> .</returns>
        public static SimpleExpressions EqProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName,ComparisonWhere.EqualProperty);
        }

        /// <summary>
        /// Apply an "not equal" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="EqPropertyExpression"/> .</returns>
        public static SimpleExpressions NotEqProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName, ComparisonWhere.NotEqProperty);
        }

        /// <summary>
        /// Apply a "greater than" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="LtPropertyExpression"/> .</returns>
        public static SimpleExpressions GtProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName, ComparisonWhere.GtProperty);
        }

        /// <summary>
        /// Apply a "greater than or equal" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="LePropertyExpression"/> .</returns>
        public static SimpleExpressions GeProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName, ComparisonWhere.GeProperty);
        }
        /// <summary>
        /// Apply a "less than" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="LtPropertyExpression"/> .</returns>
        public static SimpleExpressions LtProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName, ComparisonWhere.LtProperty);
        }

        /// <summary>
        /// Apply a "less than or equal" constraint to two properties
        /// </summary>
        /// <param name="propertyName">The lhs Property Name</param>
        /// <param name="otherPropertyName">The rhs Property Name</param>
        /// <returns>A <see cref="LePropertyExpression"/> .</returns>
        public static SimpleExpressions LeProperty(string propertyName, string otherPropertyName)
        {
            return new SimpleExpressions(propertyName, otherPropertyName, ComparisonWhere.LeProperty);
        }

        /// <summary>
        /// Apply an "is not null" constraint to the named property
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <returns>A <see cref="NotNullExpression" />.</returns>
        public static SimpleExpressions IsNotNull(string propertyName)
        {
            return new SimpleExpressions(propertyName, ComparisonWhere.IsNotNull);
        }

        /// <summary>
        /// Apply an "is not empty" constraint to the named property 
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <returns>A <see cref="IsNotEmptyExpression" />.</returns>
        public static SimpleExpressions IsNotEmpty(string propertyName)
        {
            return new SimpleExpressions(propertyName, ComparisonWhere.IsNotEmpty);
        }

        /// <summary>
        /// Apply an "is not empty" constraint to the named property 
        /// </summary>
        /// <param name="propertyName">The name of the Property in the class.</param>
        /// <returns>A <see cref="IsEmptyExpression" />.</returns>
        public static SimpleExpressions IsEmpty(string propertyName)
        {
            return new SimpleExpressions(propertyName, ComparisonWhere.IsEmpty);
        }

        /// <summary>
        /// Return the conjunction of two expressions
        /// </summary>
        /// <param name="lhs">The Expression to use as the Left Hand Side.</param>
        /// <param name="rhs">The Expression to use as the Right Hand Side.</param>
        /// <returns>An <see cref="AndExpression" />.</returns>
        public static AbstractExpressions And(AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new AndExpressions(lhs, rhs);
        }

        /// <summary>
        /// Return the disjuction of two expressions
        /// </summary>
        /// <param name="lhs">The Expression to use as the Left Hand Side.</param>
        /// <param name="rhs">The Expression to use as the Right Hand Side.</param>
        /// <returns>An <see cref="OrExpression" />.</returns>
        public static AbstractExpressions Or(AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new OrExpressions(lhs, rhs);
        }
    }
}
