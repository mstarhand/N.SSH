﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public class SimpleExpressions : LExpressions
    {
        public SimpleExpressions(string propertyName, ComparisonWhere op)
        {
            IExpression ex = new Expression();
            ex.ColumnName = propertyName;
            ex.ComparisonWhere = op;
            ExpressionList.Add(ex);
        }
        public SimpleExpressions(string propertyName,string propertyName2, ComparisonWhere op)
        {
            IExpression ex = new Expression();
            ex.ColumnName = propertyName;
            ex.ColumnName2 = propertyName2;
            ex.ComparisonWhere = op;
            ExpressionList.Add(ex);
        }
        public SimpleExpressions(string propertyName, object value, ComparisonWhere op)
		{
            IExpression ex = new Expression();
            ex.ColumnName = propertyName;
            ex.ParamStartValue = value;
            ex.ComparisonWhere = op;
            ExpressionList.Add(ex);
		}
        public SimpleExpressions(string propertyName, object[] values, ComparisonWhere op)
        {
            IExpression ex = new Expression();
            ex.ColumnName = propertyName;
            ex.ParamValues = values;
            ex.ComparisonWhere = op;
            ExpressionList.Add(ex);
        }
        public SimpleExpressions(string propertyName, object value1,object value2, ComparisonWhere op)
        {
            IExpression ex = new Expression();
            ex.ColumnName = propertyName;
            ex.ParamStartValue = value1;
            ex.ParamEndValue = value2;
            ex.ComparisonWhere = op;
            ExpressionList.Add(ex);
        }
    }
}
