﻿using Domain;
using Domain.IDomain.IDomian;
using PX.Core;
using PX.Core.List;
using System;
using PX.Code.Base;
namespace DAO.IDAO
{
    public interface Isys_role_actionDAO: IEntityDAO<sys_role_action>
    {
    }
}

