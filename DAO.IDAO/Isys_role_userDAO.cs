﻿using Domain;
using Domain.IDomain.IDomian;
using PX.Core;
using PX.Core.List;
using System;
using PX.Code.Base;
namespace DAO.IDAO
{
    public interface Isys_role_userDAO: IEntityDAO<sys_role_user>
    {
    }
}

